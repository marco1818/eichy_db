TRUNCATE TABLE CATEGORIAS;
TRUNCATE TABLE INSUMOS;
TRUNCATE TABLE INSUMOSXPRODUCTO;
TRUNCATE TABLE PRODUCTOS;
TRUNCATE TABLE TICKETS;
TRUNCATE TABLE TICKETSDETALLE;
UPDATE FOLIADORA
    SET FOLIOID = 0;

COMMIT;   
