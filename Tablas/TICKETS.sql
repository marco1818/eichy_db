CREATE TABLE TICKETS(
    FOLIOID             NUMBER(20,0),
    TOTAL               NUMBER(20,2),
    FECHA               DATE,
    USUARIOID           NUMBER(10,0),
    CAJAID              NUMBER(10,0),
    AUD_USUARIOID       NUMBER(10,0),
    AUD_FECHA           DATE,
    AUD_NUMTRANSACCION  NUMBER(20,0)
);