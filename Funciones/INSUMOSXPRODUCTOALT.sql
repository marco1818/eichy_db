CREATE OR REPLACE FUNCTION INSUMOSXPRODUCTOALT(
    PAR_PRODUCTOID          INSUMOSXPRODUCTO.PRODUCTOID%TYPE,
    PAR_INSUMOID            INSUMOSXPRODUCTO.INSUMOID%TYPE,
    PAR_UNIDADES            INSUMOSXPRODUCTO.UNIDADES%TYPE,
    PAR_SELECCIONABLE       INSUMOSXPRODUCTO.SELECCIONABLE%TYPE,
    PAR_USUARIOID           INSUMOSXPRODUCTO.AUD_USUARIOID%TYPE,
    PAR_NUMTRANSACCION      INSUMOSXPRODUCTO.AUD_NUMTRANSACCION%TYPE
)
    RETURN  SYS_REFCURSOR
IS
--  DECLARACION DE CURSORES.
CUR_SALIDA      SYS_REFCURSOR;
--  DECLARACION DE CONSTANTES.
CON_0               CONSTANT    NUMBER(1,0) :=  0;
CON_1               CONSTANT    NUMBER(1,0) :=  1;
CON_2               CONSTANT    NUMBER(1,0) :=  2;
CON_3               CONSTANT    NUMBER(1,0) :=  3;
CON_4               CONSTANT    NUMBER(1,0) :=  4;
CON_999             CONSTANT    NUMBER(3,0) :=  999;
CON_ELIMINAR        CONSTANT    NUMBER(1,0) :=  1;
CON_INSERTAR        CONSTANT    NUMBER(1,0) :=  2;
--  DECLARACION DE VARIABLES
VL_MENSAJEID        NUMBER(20,0);
VL_MENSAJE          VARCHAR2(250);
--  DECLARACION DE EXCEPCIONES
EXCEPCION_VALIDACION    EXCEPTION;
BEGIN

IF( NVL(PAR_PRODUCTOID, CON_0)  =   CON_0)  THEN
    VL_MENSAJEID    :=  CON_1;
    VL_MENSAJE      :=  'EL ID DEL PRODUCTO ESTA VACIO.';
    RAISE EXCEPCION_VALIDACION;
END IF;

IF( NVL(PAR_INSUMOID, CON_0)    =   CON_0)  THEN
    VL_MENSAJEID    :=  CON_2;
    VL_MENSAJE      :=  'EL ID DEL INSUMO ESTA VACIO.';
    RAISE EXCEPCION_VALIDACION;
END IF;

IF( NVL(PAR_UNIDADES, CON_0)    =   CON_0)  THEN
    VL_MENSAJEID    :=  CON_3;
    VL_MENSAJE      :=  'EL NUMERO DE UNIDADES ESTA VACIO.';
    RAISE   EXCEPCION_VALIDACION;
END IF;

INSERT INTO INSUMOSXPRODUCTO    (   PRODUCTOID,     INSUMOID,       UNIDADES,       SELECCIONABLE,      AUD_USUARIOID,  
                                    AUD_FECHA,      AUD_NUMTRANSACCION)
                        VALUES  (   PAR_PRODUCTOID, PAR_INSUMOID,   PAR_UNIDADES,   PAR_SELECCIONABLE,  PAR_USUARIOID,
                                    SYSDATE,        PAR_NUMTRANSACCION);
                                    
VL_MENSAJEID    :=  CON_0;
VL_MENSAJE      :=  'SE ASOCIO DE MANERA CORRECTA EL INSUMO: '||PAR_INSUMOID||'  AL PRODUCTO: '||PAR_PRODUCTOID;

COMMIT;

OPEN    CUR_SALIDA  FOR
    SELECT  VL_MENSAJEID    MENSAJEID,
            VL_MENSAJE      MENSAJE
        FROM DUAL;

RETURN  CUR_SALIDA;

EXCEPTION
    WHEN    EXCEPCION_VALIDACION    THEN
        OPEN    CUR_SALIDA  FOR
            SELECT  VL_MENSAJEID    MENSAJEID,
                    VL_MENSAJE      MENSAJE
                FROM DUAL;
        
        RETURN  CUR_SALIDA;
        
    WHEN    OTHERS  THEN
        ROLLBACK;
        VL_MENSAJEID    :=  CON_999;
        VL_MENSAJE      :=  'OCURRIO UN DETALLE AL REALIZAR LA OPERACION: '||SQLERRM||'--'||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
        
        OPEN    CUR_SALIDA  FOR
            SELECT  VL_MENSAJEID    MENSAJEID,
                VL_MENSAJE      MENSAJE
                FROM DUAL;
    
        RETURN  CUR_SALIDA;

END INSUMOSXPRODUCTOALT;