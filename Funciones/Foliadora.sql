CREATE OR REPLACE FUNCTION FOLIADORAACT(
    PAR_OPCIONID    NUMBER,    
    PAR_PROCESOID   FOLIADORA.PROCESOID%TYPE,
    PAR_FOLIOID     FOLIADORA.FOLIOID%TYPE,
    PAR_DESCRIPCION FOLIADORA.DESCRIPCION%TYPE
)
    RETURN  SYS_REFCURSOR
IS

--  DECLARACION DE CURSORES
CUR_SALIDA          SYS_REFCURSOR;
--  DECLARACION DE CONSTANTES
CON_INSERTAR        CONSTANT    NUMBER(1,0) :=  1;
CON_OBTENER         CONSTANT    NUMBER(1,0) :=  2;
CON_ACTUALIZA       CONSTANT    NUMBER(1,0) :=  3;
CON_ELIMINAR        CONSTANT    NUMBER(1,0) :=  4;
CON_0               CONSTANT    NUMBER(1,0) :=  0;
CON_1               CONSTANT    NUMBER(1,0) :=  1;
CON_999             CONSTANT    NUMBER(3,0) :=  999;
--  DECLARACION DE VARIABLES
VAR_MENSAJEID       NUMBER(20,0);
VAR_MENSAJE         VARCHAR2(200);
VAR_EXISTE          NUMBER(20,0);

PRAGMA AUTONOMOUS_TRANSACTION;
    
BEGIN

    CASE    PAR_OPCIONID
        WHEN    CON_INSERTAR    THEN
            INSERT  INTO    FOLIADORA   (PROCESOID,     FOLIOID,        DESCRIPCION)
                                VALUES  (PAR_PROCESOID, PAR_FOLIOID,    PAR_DESCRIPCION);

            VAR_MENSAJEID   :=  CON_0;
            VAR_MENSAJE     :=  'SE REGISTRO CORRECTAMENTE EL PROCESO.';
        WHEN    CON_OBTENER     THEN
            
            SELECT  COUNT(CON_1)
            INTO    VAR_EXISTE
                FROM    FOLIADORA
                WHERE   PROCESOID    =   PAR_PROCESOID;
                    
            IF( VAR_EXISTE > CON_0)    THEN
                
                SELECT      (FOLIOID+CON_1)
                    INTO    VAR_MENSAJEID
                    FROM    FOLIADORA
                    WHERE   PROCESOID    =   PAR_PROCESOID;
                
                UPDATE  FOLIADORA
                    SET FOLIOID =   (FOLIOID+CON_1)
                    WHERE   PROCESOID    =   PAR_PROCESOID;
                    
                VAR_MENSAJE     :=  'FOLIO OBTENIDO EXITOSAMENTE.';
            ELSE
                VAR_MENSAJEID   :=  CON_999;
                VAR_MENSAJE     :=  'NO SE ENCONTRO EL PROCESO.';
            END IF;
        ELSE
            VAR_MENSAJEID   :=  CON_0;
            VAR_MENSAJE     :=  'NO SE EJECUTO NINGUNA ACCION.';
        END CASE;
        
        COMMIT;
        
        OPEN    CUR_SALIDA  FOR
            SELECT  VAR_MENSAJEID MENSAJEID,
                    VAR_MENSAJE MENSAJE
                FROM    DUAL;
        
        RETURN CUR_SALIDA;


EXCEPTION
    WHEN    OTHERS  THEN
        DBMS_OUTPUT.PUT_LINE(SQLERRM);
        DBMS_OUTPUT.put_line (DBMS_UTILITY.format_error_backtrace); 
        ROLLBACK;

END FOLIADORAACT;